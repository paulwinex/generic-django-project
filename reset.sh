#!/usr/bin/env bash
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh
workon dev

export DJANGO_SETTINGS_MODULE="main.settings"
echo "---------------------------------------------------"
echo "RESET DATABASE"
echo "---------------------------------------------------"
export PGPASSWORD="cgnMegaDbPass4"
psql -U paul cgn_db -t -c "select 'drop table \"' || tablename || '\" cascade;' from pg_tables where schemaname = 'public'"  | psql -U paul cgn_db

python ./manage.py migrate
python ./manage.py migrate --run-syncdb
python ./manage.py collectstatic --noinput
echo "---------------------------------------------------"
echo "INIT DEMO DATA"
echo "---------------------------------------------------"
python ./init.py
