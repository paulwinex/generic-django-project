from __future__ import unicode_literals
from custom_user.models import AbstractEmailUser
from phonenumber_field.modelfields import PhoneNumberField
from django.db import models


class GenericUser(AbstractEmailUser):
    """
    Example of an EmailUser with a new field date_of_birth
    """
    firstName = models.CharField('First Name', max_length=32, blank=True, null=True)
    lastName = models.CharField('Last Name', max_length=32, blank=True, null=True)
    phone = PhoneNumberField('Phone', blank=True)
    url = models.URLField('Social URL', max_length=300, blank=True, null=True)
