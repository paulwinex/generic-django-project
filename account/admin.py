from django.contrib import admin
from custom_user.admin import EmailUserAdmin
from .models import GenericUser


class GenericUserAdmin(EmailUserAdmin):

    def get_fieldsets(self, request, obj=None):
        sets = super(GenericUserAdmin, self).get_fieldsets(request, obj)
        fieldsets = (
            ('Ninja User', {
                'fields': ('firstName', 'lastName', 'phone', 'url'),
            }),
        )
        return fieldsets+sets

# Register your models here.
admin.site.register(GenericUser, GenericUserAdmin)