import django
django.setup()
from account.models import GenericUser
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.core.files import File
from django.conf import settings
import random, os

password = '123'


def create_admin():
    u = GenericUser.objects.create_superuser('admin@admin.com', password)


def start():
    create_admin()

if __name__ == '__main__':
    start()
