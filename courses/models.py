from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from django.utils.deconstruct import deconstructible
from taggit.managers import TaggableManager
import os


@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        filename = instance.slug + os.path.splitext(filename)[1]
        return os.path.join(self.path, filename)


class Course(models.Model):
    images_path = PathAndRename('images/')

    title = models.CharField('Title', max_length=300, blank=True, null=True)
    slug = models.CharField('Slug', max_length=10, blank=True, null=True)
    image = ThumbnailerImageField('Image', upload_to=images_path,
                                  resize_source={"size": (200, 200), 'crop': True},
                                  blank=True)
    creation_date = models.DateTimeField('Creation date', auto_now=True, editable=False, blank=False, null=False)
    cost = models.PositiveIntegerField('Cost', default=0, blank=False, null=False)
    tags = TaggableManager()
